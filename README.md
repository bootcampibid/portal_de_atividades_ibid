# Projeto de Exemplo ASP.NET com Pain�is de Formul�rio

![Apex_1699283387684](https://github.com/zeldinha00/EstudosDio/assets/14182590/1159af2f-6c30-4077-9c69-878253fa81ff)

Este � um projeto de exemplo em ASP.NET que demonstra o uso de pain�is de formul�rio para coletar informa��es pessoais, de endere�o e de login de um usu�rio. O projeto apresenta a funcionalidade de valida��o de formul�rio, redirecionamento de p�gina e exibi��o dos dados preenchidos em uma p�gina separada.

## Funcionalidades Implementadas

- Coleta de informa��es pessoais, incluindo nome, sobrenome, g�nero e n�mero de celular.
- Coleta de informa��es de endere�o, incluindo endere�o, cidade e CEP.
- Coleta de informa��es de login, incluindo nome de usu�rio e senha.
- Valida��o de formul�rio para garantir que todos os campos sejam preenchidos antes de avan�ar para o pr�ximo painel.

## Arquivos Importantes

- `index.aspx`: P�gina inicial de login do usu�rio.
- `ProjetosIBID.aspx`: P�gina principal que cont�m os pain�is de formul�rio e a l�gica de valida��o.
- `styles.css`: Arquivo CSS para estilizar a apresenta��o da p�gina.

## Como Executar o Projeto

1. Clone o reposit�rio para o seu ambiente local.
2. Abra o projeto em uma IDE compat�vel com ASP.NET.
3. Execute o projeto em um servidor web local.
4. Preencha os formul�rios com os dados solicitados e siga as instru��es na p�gina.

## Contribui��o

Este projeto foi criado como um exemplo educacional **IBID** para demonstrar o uso de pain�is de formul�rio em ASP.NET. Contribui��es s�o bem-vindas para melhorias e aprimoramentos adicionais.

## Licen�a

Este projeto � licenciado sob a licen�a MIT - consulte o arquivo [LICENSE.md](LICENSE.md) para obter mais detalhes.