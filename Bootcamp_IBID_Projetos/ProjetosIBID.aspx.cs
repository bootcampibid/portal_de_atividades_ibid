﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bootcamp_IBID_Projetos
{
    public partial class ProjetosIBID : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        // ===== TRATAMENTO PAINEIS =====
        protected void BtnPanel1_Click(object sender, EventArgs e)
        {

            // Desativar a cor do Panel1
            BtnPanel1.CssClass = "btn btn-primary active m-1";

            // Ativar a cor do Panel2
            BtnPanel2.CssClass = "btn btn-primary m-1";

            // Desativar a cor do Panel3
            BtnPanel3.CssClass = "btn btn-primary m-1";

            Panel1.Visible = true;
            Panel2.Visible = false;
            Panel3.Visible = false;
        }

        protected void BtnPanel2_Click(object sender, EventArgs e)
        {

            // Desativar a cor do Panel1
            BtnPanel1.CssClass = "btn btn-primary m-1";

            // Ativar a cor do Panel2
            BtnPanel2.CssClass = "btn btn-primary active m-1";

            // Desativar a cor do Panel3
            BtnPanel3.CssClass = "btn btn-primary m-1";

            Panel1.Visible = false;
            Panel2.Visible = true;
            Panel3.Visible = false;
        }

        protected void BtnPanel3_Click(object sender, EventArgs e)
        {
            // Desativar a cor do Panel1
            BtnPanel1.CssClass = "btn btn-primary m-1";

            // Ativar a cor do Panel2
            BtnPanel2.CssClass = "btn btn-primary  m-1";

            // Desativar a cor do Panel3
            BtnPanel3.CssClass = "btn btn-primary active m-1";

            Panel1.Visible = false;
            Panel2.Visible = false;
            Panel3.Visible = true;
        }

        // ===== TRATAMENTO PAINEL CALCULADORA =====
        protected void CalculateButton_Click(object sender, EventArgs e)
        {
            // Tratamento se o usuario digitar strin ou outro tipo de caracter e passando o numero para double.
            if (!double.TryParse(lblNumero1.Text.Replace(".", ","), out double num1) || !double.TryParse(lblNumero2.Text.Replace(".", ","), out double num2))
            {
                lblResultado.Text = "Por favor, insira números válidos.";
                return;
            }
            // Selecionando o resultado do IF e colocando em um Switch
            string operacao = ddlOperacao.SelectedValue;
            double resultado = 0;
            // Selecionando a operação e resulvendo os casos
            switch (operacao)
            {
                case "somar":
                    resultado = num1 + num2;
                    break;
                case "subtrair":
                    resultado = num1 - num2;
                    break;
                case "multiplicar":
                    resultado = num1 * num2;
                    break;
                case "dividir":
                    if (num2 != 0)
                    {
                        resultado = num1 / num2;
                    }
                    else
                    {
                        // corrigindo divisão por ZERO
                        lblResultado.Text = "Erro: Divisão por zero.";
                        return;
                    }
                    break;
            }
            // Mostrando o resultado.
            lblResultado.Text = "Resultado: " + resultado.ToString().Replace(".", ",");
        }

        // ===== TRATAMENTO PAINEL SELECIONAR DIAS =====
        protected void ButtonConfirm_Click(object sender, EventArgs e)
        {
            bool diaSelecionado = false;

            foreach (ListItem item in CheckBoxListDays.Items)
            {
                if (item.Selected)
                {
                    diaSelecionado = true;
                    break;
                }
            }

            if (!diaSelecionado)
            {
                lblDiasSelecionadosNaTela.Text = "Por favor, selecione pelo menos um dia.";
            }
            else
            {
                string diasSelecionados = "Dia(s) Selecionado(s): ";
                foreach (ListItem item in CheckBoxListDays.Items)
                {
                    if (item.Selected)
                    {
                        diasSelecionados += item.Text + ", ";
                    }
                }
                lblDiasSelecionadosNaTela.Text = diasSelecionados;
            }
        }
        // ===== TRATAMENTO PAINEL INFORMAÇÕES =====
        protected void BtnAvancaEndereco_Click(object sender, EventArgs e)
        {
            // Validar se todos campos estão preenchidos 
            if (string.IsNullOrEmpty(txtNome.Text) || string.IsNullOrEmpty(txtSobronome.Text) || string.IsNullOrEmpty(txtGenero.Text) || string.IsNullOrEmpty(txtNumero.Text))
            {
                lblErrorNaTela.Text = "❌ Preencha todos os campos";
                return;
            }

            // Validar se os campos de Nome, Sobrenome e Gênero não estão vazios
            //
            Regex regexNome = new Regex("^[a-zA-Z ]+$");
            if (!regexNome.IsMatch(txtNome.Text))
            {
                txtErrorNome.Text = "❌ O campo NOME deve conter apenas letras.";
                return;
            }

            if (!regexNome.IsMatch(txtSobronome.Text))
            {
                txtErrorNomeSobrenome.Text = "❌ O campo SOBRENOME deve conter apenas letras.";
                return;
            }

            // Validar se o número de celular tem exatamente 11 dígitos
            if (txtNumero.Text.Length != 11)
            {
                txtErrorCelular.Text = "❌ O número de CELULAR deve ter exatamente 11 dígitos.";
                return;
            }

            // Validar se o campo celular contém apenas números

            //  ^: Representa o início da string.
            // [0 - 9]: Representa a faixa de caracteres de 0 a 9, o que significa que aceita qualquer dígito de 0 a 9.
            // +: Indica que o dígito de 0 a 9 pode ocorrer uma ou mais vezes na string.
            // $: Representa o final da string.

            Regex regex = new Regex(@"^[0-9]+$");
            if (!regex.IsMatch(txtNumero.Text))
            {
                lblErrorNaTela.Text = "❌ O campo CELULAR deve conter apenas números.";
                return;
            }

            //// Formatando o número de celular
            //string celular = txtNumero.Text;
            //celular = string.Format("({0}) {1}-{2}", celular.Substring(0, 2), celular.Substring(2, 5), celular.Substring(7));

            // Ação a ser executada se todos os campos estiverem preenchidos corretamente
            Panel5.Visible = false;
            Panel6.Visible = true;
            Panel7.Visible = false;
        }

        // TRATATIVA BOTÃO VOLTAR AS INFORMAÇÕES PESSOAIS
        protected void BtnVoltarInfoPessoais_Click(object sender, EventArgs e)
        {
            Panel5.Visible = true;
            Panel6.Visible = false;
            Panel7.Visible = false;
        }

        // TRATATIVA ENDEREÇO
        protected void BtnAvanacaLogin_Click(object sender, EventArgs e)
        {
            // Validar se os campos estão todos preenchidos 
            if (string.IsNullOrEmpty(txtEndereco.Text) || string.IsNullOrEmpty(txtCidade.Text) || string.IsNullOrEmpty(txtCEP.Text))
            {
                lblErrorNaTelaEndereco.Text = "❌ Preencha todos os campos";
                return;
            }

            // Validar o campo Cidade para ter apenas String ( AFABETICO )
            Regex regexCidade = new Regex("^[a-zA-Z ]+$");
            if (!regexCidade.IsMatch(txtCidade.Text))
            {
                txtErrorCidade.Text = "❌ O campo CIDADE deve conter apenas letras.";
                return;
            }

            // Validar se os celular tem apenas números
            Regex regex = new Regex(@"^[0-9]+$");
            if (!regex.IsMatch(txtCEP.Text))
            {
                txtErrorCEP.Text = "❌ O campo CEP deve conter apenas números.";
                return;
            }

            // Validar se o número de CEP tem exatamente 11 dígitos
            if (txtCEP.Text.Length != 8)
            {
                txtErrorCEP.Text = "❌ O número de CEP deve ter exatamente 08 dígitos.";
                return;
            }
            // Ação a ser executada se todos os campos estiverem preenchidos corretamente
            Panel5.Visible = false;
            Panel6.Visible = false;
            Panel7.Visible = true;
        }

        // // TRATATIVA BOTÃO VOLTAR AS ENDEREÇO
        protected void BtnVoltarEndereco_Click(object sender, EventArgs e)
        {
            Panel5.Visible = false;
            Panel6.Visible = true;
            Panel7.Visible = false;
        }

        // TELA QUE MOSTRA OS RESULTADOS DIGITADOS 
        protected void BtnEnviar_Click1(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLogin.Text) || string.IsNullOrEmpty(txtSenha.Text))
            {
                // ALERT EM JAVASCRIPT
                //Response.Write("<script>alert('Por favor, preencha todos os campos antes de prosseguir.');</script>");

                lblErrorNaTelaLogin.Text = "❌ Preencha todos os campos";
                return;
            }
            if (txtSenha.Text.Length < 6)
            {
                lblErrorNaTelaLogin.Text = "❌ A senha deve conter mais de 06 dígitos.";
                return;
            }

            lblNomeDigitado.InnerText = $"{txtNome.Text} {txtSobronome.Text}";
            lblGeneroDigitado.InnerText = txtGenero.Text;
            lblCelularDigitado.InnerText = txtNumero.Text;
            lblEnderecoDigitado.InnerText = txtEndereco.Text;
            lblCidadeDigitado.InnerText = txtCidade.Text;
            lblCepDigitado.InnerText = txtCEP.Text;
            lblLoginDigitado.InnerText = txtLogin.Text;
            lblSenhaDigitado.InnerText = "********"; // Não exibimos senhas

            // Ocultando os painéis anteriores e exibindo Panel5
            Panel5.Visible = false;
            Panel6.Visible = false;
            Panel7.Visible = false;
            Panel8.Visible = true;
        }


    }
}