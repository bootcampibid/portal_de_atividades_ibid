﻿<%@ Page Language="C#" CodeBehind="index.aspx.cs" Inherits="Bootcamp_IBID_Projetos.WebForm1" EnableEventValidation="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>IBID Login</title>
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous" />
    <!-- Folha de Estilo -->
    <link rel="stylesheet" href="styleIndex.css" />
</head>
<body>
    <form id="form1" runat="server">

        
        <div class="container">
            <div class="mb-4">
                <asp:Image ID="Image4" runat="server" ImageUrl="https://www.ibid.com.br/images/logotipo.png"/>
            </div>
            
            <div class="card" style="width: 25rem;">
                <div class="card-body">
                    <div>
                        <div class="d-flex justify-content-end mb-3">
                            <asp:Image ID="Image2" runat="server" ImageUrl="https://teste.ibid.com.br/Core/images/flags/brasil_24.png" CssClass="mx-2 mt-3" />
                            <asp:Image ID="Image3" runat="server" ImageUrl="https://teste.ibid.com.br/Core/images/flags/estadosunidos_24.png" CssClass="mx-2 mt-3" />
                        </div>

                        <!-- Login -->
                        <div class="mb-3">
                            <label class="form-label fw-bold">Usuário / CNPJ</label>
                            <asp:TextBox class="form-control" ID="TxtLogin" runat="server" BackColor="#E8F0FE"></asp:TextBox>
                        </div>
                        <!-- Senha -->
                        <div class="mb-3">
                            <label class="form-label fw-bold">Senha</label>
                            <asp:TextBox class="form-control" ID="TxtSenha" runat="server" TextMode="Password" BackColor="#E8F0FE"></asp:TextBox>
                        </div>

                        <!-- link recuperação de senha -->
                        <div class="mb-3">
                            <a href="#" class="text-decoration-none">Esqueceu sua senha? Primeiro acesso?</a>
                        </div>

                        <!-- Botão Acessar -->
                        <div class="d-grid gap-2">
                            <asp:Button class="btn btn-success" ID="Button2" runat="server" Text="Acessar" OnClick="Button2_Click" />
                            <hr />
                            <asp:Button class="btn btn-danger" ID="Button1" runat="server" Text="Seja nosso fornecedor - Cadastre-se aqui!" />

                        </div>

                        <!-- Campo Menssagem de ERRO -->
                        <div style="text-align: center; color: #ff0000; margin-top: 10px" class="auto-style3">
                            <asp:Label ID="lblErroLoginIndex" runat="server" ForeColor="#ff0000"></asp:Label>
                        </div>

                        <div class="mt-4">
                            <asp:Image ID="Image1" runat="server" ImageUrl="https://teste.ibid.com.br/core/images/logo/powered.png" CssClass="d-block mx-auto" />
                        </div>

                    </div>
                </div>
            </div>
        </div>



    </form>
    <!-- Script -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>
