﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjetosIBID.aspx.cs" Inherits="Bootcamp_IBID_Projetos.ProjetosIBID" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>:.Projetos Bootcamp IBID :.</title>
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous" />
    <!-- Folha de Estilo -->
    <link rel="stylesheet" href="style.css" />
    <style type="text/css">
        .auto-style1 {
            margin: 0 auto;
            text-align: center;
            width: 402px;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
            background-color: #f9f9f9;
        }

        .auto-style2 {
            --bs-gutter-x: 1.5rem;
            --bs-gutter-y: 0;
            width: 100%;
            padding-right: calc(var(--bs-gutter-x) * .5);
            padding-left: calc(var(--bs-gutter-x) * .5);
            margin-right: auto;
            margin-left: auto;
            font-weight: 500;
        }
    </style>
</head>
<body>

    <!-- Atalho para procurar no HTML -->
<!-- 
    - Painel 1: Panel1 Calculadora
    - Painel 2: Panel2 Selecionar Dias
    - Painel 3: Panel3 Formulario Dados
-->


    <form id="form1" runat="server">
        <nav class="navbar navbar-light bg-light">
            <div class="auto-style2 text-center">
                <h1>IBID - Bootcamp Atvidades</h1>
            </div>
        </nav>
        <!-- MENU TOP -->
        <div class="card text-center">
            <div class="card-header ">
                <ul class="nav nav-pills card-header-pills d-flex justify-content-center">
                    <li class="nav-item">
                        <asp:Button ID="BtnPanel1" runat="server" class="btn btn-primary active m-1" Text="Calculadora" OnClick="BtnPanel1_Click" />
                    </li>
                    <li class="nav-item">
                        <asp:Button ID="BtnPanel2" runat="server" class="btn btn-primary  m-1" Text="Check List" OnClick="BtnPanel2_Click" />
                    </li>
                    <li class="nav-item">
                        <asp:Button ID="BtnPanel3" runat="server" class="btn btn-primary  m-1" Text="Controle Panel" OnClick="BtnPanel3_Click" />
                    </li>
                </ul>
            </div>
        </div>

        <!-- Painel Calculadora -->

        <asp:Panel ID="Panel1" runat="server">
            <div class="d-flex justify-content-center align-items-baseline mt-5">
                <div class="auto-style1">
                    <h1>Calculadora 🧮</h1>
                    <p>
                        &nbsp;
                    </p>
                    <asp:Label CssClass="result-box" ID="lblResultado" runat="server" Font-Bold="True" CsCalass="btn_resultado">Faça suas operações!</asp:Label>
                    <br />
                    <br />
                    <br />
                    <asp:TextBox ID="lblNumero1" runat="server" CssClass="textbox" placeholder="Insira um valor"></asp:TextBox>
                    <asp:DropDownList ID="ddlOperacao" runat="server" CssClass="dropdown">
                        <asp:ListItem Text="Selecione uma operação"></asp:ListItem>
                        <asp:ListItem Text="[ + ] Adição" Value="somar"></asp:ListItem>
                        <asp:ListItem Text="[ - ] Subtração" Value="subtrair"></asp:ListItem>
                        <asp:ListItem Text="[ x ] Multiplicação" Value="multiplicar"></asp:ListItem>
                        <asp:ListItem Text="[ / ] Divisão" Value="dividir"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="lblNumero2" runat="server" class="textbox" placeholder="Insira um valor"></asp:TextBox>
                    <asp:Button CssClass="button" ID="CalculateButton" runat="server" Text="Calcular" OnClick="CalculateButton_Click" />
                </div>
            </div>
        </asp:Panel>

        <!-- PAINEL DIAS DA SEMANA -->
        <asp:Panel ID="Panel2" runat="server" Visible="false">

            <div class="d-flex justify-content-center align-items-baseline mt-5" style="height: 100vh;">
                <div class="card text-center" style="width: 50%;">
                    <div class="card-header">
                        <h1>Formulário Dias 📌</h1>
                    </div>
                    <div class="card-body bg_usuario">
                        <h5 class="card-title">Selecione o(s) dia(s)</h5>
                        <p class="card-title">
                            &nbsp;</p>
                        <div class="text-center">
                            <asp:CheckBoxList ID="CheckBoxListDays" runat="server" CssClass="custom-checkbox-list" Height="36px" RepeatDirection="Horizontal" TextAlign="Left" Width="927px">
                                <asp:ListItem Text="Domingo" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Segunda-feira" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Terça-feira" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Quarta-feira" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Quinta-feira" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Sexta-feira" Value="5"></asp:ListItem>
                                <asp:ListItem Text="Sábado" Value="6"></asp:ListItem>
                            </asp:CheckBoxList>
                        </div>

                        <br />
                        <asp:Button ID="ButtonConfirm" runat="server" Text="Confirmar Dias" OnClick="ButtonConfirm_Click" CssClass="button" />
                        <br />
                        <br />
                        <asp:Label CssClass="result-box-dia" ID="lblDiasSelecionadosNaTela" runat="server" Height="72px" Width="553px"></asp:Label>
                    </div>
                </div>
            </div>
        </asp:Panel>

        <!-- PAINEL INFORMAÇÔES -->

        <asp:Panel ID="Panel3" runat="server" Visible="false">

            <div class="d-flex justify-content-center align-items-baseline mt-5" style="height: 100vh;">
                <div class="card" style="width: 50%;">
                    <div class="card-header text-center">
                        <h1>Cadastro de Usuário 📝</h1>
                    </div>
                    <div class="card-body bg_usuario">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 100%">
                                    <!-- PAINEL DE PAI -->
                                    <asp:Panel ID="Panel4" runat="server">

                                        <!-- ============ PAINEL DE DADOS PESSOAIS ============ -->
                                        <asp:Panel ID="Panel5" runat="server">
                                            <form>
                                                <!-- Nome -->
                                                <div class="mb-3">
                                                    <label class="form-label fw-bold">Nome</label>
                                                    <asp:TextBox class="form-control" ID="txtNome" runat="server"></asp:TextBox>
                                                    <!-- Campo Menssagem de ERRO nome-->
                                                    <asp:Label ID="txtErrorNome" runat="server" ForeColor="#ff0000"></asp:Label>
                                                </div>
                                                <!-- Sobrenome -->
                                                <div class="mb-3">
                                                    <label class="form-label fw-bold">Sobrenome</label>
                                                    <asp:TextBox class="form-control" ID="txtSobronome" runat="server"></asp:TextBox>
                                                    <!-- Campo Menssagem de ERRO Sobrenome-->
                                                    <asp:Label ID="txtErrorNomeSobrenome" runat="server" ForeColor="#ff0000"></asp:Label>
                                                </div>
                                                <!-- Gênero -->
                                                <div class="mb-3">
                                                    <label class="form-label fw-bold">Gênero</label>
                                                    <asp:TextBox class="form-control" ID="txtGenero" runat="server"></asp:TextBox>
                                                </div>
                                                <!-- Celular -->
                                                <div class="mb-3">
                                                    <label class="form-label fw-bold">Celular</label>
                                                    <asp:TextBox class="form-control" ID="txtNumero" runat="server"></asp:TextBox>
                                                    <asp:Label ID="txtErrorCelular" runat="server" ForeColor="#ff0000"></asp:Label>

                                                </div>
                                                <!-- Botão próximo -->
                                                <div class="d-flex justify-content-end mb-3">
                                                    <asp:Button class="btn btn-secondary btn-lg" ID="BtnAvancaEndereco" runat="server" Width="131px" Text="Próximo" OnClick="BtnAvancaEndereco_Click" />
                                                </div>
                                                <!-- Campo Menssagem de ERRO -->
                                                <div style="text-align: center; color: #ff0000; margin-top: 10px" class="auto-style3">
                                                    <asp:Label ID="lblErrorNaTela" runat="server" ForeColor="#ff0000"></asp:Label>
                                                </div>

                                            </form>
                                        </asp:Panel>

                                        <!-- ========== PAINEL DE ENDEREÇO ========== -->
                                        <asp:Panel ID="Panel6" runat="server" Visible="false">
                                            <form>
                                                <!-- Endereço -->
                                                <div class="mb-3">
                                                    <label class="form-label fw-bold">Endereço</label>
                                                    <asp:TextBox class="form-control" ID="txtEndereco" runat="server"></asp:TextBox>
                                                </div>
                                                <!-- Cidade -->
                                                <div class="mb-3">
                                                    <label class="form-label fw-bold">Cidade</label>
                                                    <asp:TextBox class="form-control" ID="txtCidade" runat="server"></asp:TextBox>
                                                    <!-- Campo Menssagem de ERRO Cidade-->
                                                    <asp:Label ID="txtErrorCidade" runat="server" ForeColor="#ff0000"></asp:Label>
                                                </div>
                                                <!-- CEP -->
                                                <div class="mb-3">
                                                    <label class="form-label fw-bold">CEP</label>
                                                    <asp:TextBox class="form-control" ID="txtCEP" runat="server"></asp:TextBox>
                                                    <!-- Campo Menssagem de ERRO Cidade-->
                                                    <asp:Label ID="txtErrorCEP" runat="server" ForeColor="#ff0000"></asp:Label>
                                                </div>

                                                <!-- Botão voltar / próximo -->
                                                <div class="d-flex justify-content-end mb-3">
                                                    <asp:Button class="btn btn-secondary btn-lg mx-2" ID="BtnVoltarInfoPessoais" runat="server" Width="111px" Text="Voltar" OnClick="BtnVoltarInfoPessoais_Click" />
                                                    <asp:Button class="btn btn-secondary btn-lg" ID="BtnAvanacaLogin" runat="server" Width="123px" Text="Próximo" OnClick="BtnAvanacaLogin_Click" />
                                                </div>
                                               
                                                <!-- Campo Menssagem de ERRO -->
                                                <div style="text-align: center; color: #ff0000; margin-top: 10px" class="auto-style3">
                                                    <asp:Label ID="lblErrorNaTelaEndereco" runat="server" ForeColor="#ff0000"></asp:Label>
                                                </div>
                                            </form>
                                        </asp:Panel>

                                        <!-- ========== PAINEL DE LOGIN ========== -->
                                        <asp:Panel ID="Panel7" runat="server" Visible="false">
                                            <form>
                                                <!-- Login -->
                                                <div class="mb-3">
                                                    <label class="form-label fw-bold">Login</label>
                                                    <asp:TextBox class="form-control" ID="txtLogin" runat="server"></asp:TextBox>
                                                </div>
                                                <!-- Senha -->
                                                <div class="mb-3">
                                                    <label class="form-label fw-bold">Senha</label>
                                                    <asp:TextBox class="form-control" ID="txtSenha" runat="server" TextMode="Password"></asp:TextBox>
                                                </div>

                                                <!-- Botão Voltar / Enviar -->
                                                <div class="d-flex justify-content-end mb-3">
                                                     <asp:Button class="btn btn-secondary btn-lg mx-2" ID="BtnVoltarEndereco" runat="server"  Text="Voltar" OnClick="BtnVoltarEndereco_Click" />
                                                <asp:Button class="btn btn-success btn-lg" ID="BtnEnviar" runat="server" Width="98px" Text="Enviar" OnClick="BtnEnviar_Click1" />
                                                </div>
                                               
                                                <!-- Campo Menssagem de ERRO -->
                                                <div style="text-align: center; color: #ff0000; margin-top: 10px" class="auto-style3">
                                                    <asp:Label ID="lblErrorNaTelaLogin" runat="server" ForeColor="#ff0000"></asp:Label>
                                                </div>
                                            </form>
                                        </asp:Panel>

                                        <!-- ========== PAINEL DE RESULTADOS ========== -->
                                        <asp:Panel ID="Panel8" runat="server" CssClass="result-panel" Visible="false">
                                            <p class="text-center fw-bold">Dados informado com sucesso! ✅</p>
                                            <div>
                                                <div><strong>Nome:</strong> <span id="lblNomeDigitado" runat="server"></span></div>
                                                <div><strong>Gênero:</strong> <span id="lblGeneroDigitado" runat="server"></span></div>
                                                <div><strong>Celular:</strong> <span id="lblCelularDigitado" runat="server"></span></div>
                                                <div><strong>Endereço:</strong> <span id="lblEnderecoDigitado" runat="server"></span></div>
                                                <div><strong>Cidade:</strong> <span id="lblCidadeDigitado" runat="server"></span></div>
                                                <div><strong>CEP:</strong> <span id="lblCepDigitado" runat="server"></span></div>
                                                <div><strong>Login:</strong> <span id="lblLoginDigitado" runat="server"></span></div>
                                                <div><strong>Senha:</strong> <span id="lblSenhaDigitado" runat="server"></span></div>
                                            </div>
                                            <p class="thanks-message text-center">Obrigado por usar o sistema IBID 💚</p>
                                        </asp:Panel>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </asp:Panel>


    </form>
    <!-- Script -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>
