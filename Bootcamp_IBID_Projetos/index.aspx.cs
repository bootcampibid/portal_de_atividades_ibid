﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bootcamp_IBID_Projetos
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtLogin.Text) || string.IsNullOrEmpty(TxtSenha.Text))
            {
                lblErroLoginIndex.Text = "❌ Preencha todos os campos";
                return;
            }
            else
            {
                // Se os campos de login e senha estiverem preenchidos, redirecione para a página "ProjetosIBID.aspx":
                Response.Redirect("ProjetosIBID.aspx");
            }
        }
    }
}